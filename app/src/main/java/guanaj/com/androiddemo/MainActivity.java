package guanaj.com.androiddemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import guanaj.com.library.utils.ELog;

public class MainActivity extends AppCompatActivity {

    private static final java.lang.String TAG = "MainActivity";
    private List<String> listData;
    private LayoutInflater inflater;
    private MAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inflater = getLayoutInflater();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.m_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new MAdapter();
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setRecyclerListener(new RecyclerView.RecyclerListener() {
            @Override
            public void onViewRecycled(RecyclerView.ViewHolder holder) {
                Log.i(TAG, ">>holder of position is " + holder.getAdapterPosition());
            }
        });

//        ELog.Builder builder = ELog.getBuilder(getApplicationContext());
//        builder.setLog2FileSwitch(true)
//                .setLogSwitch(true)
//                .setTag("veni").create();
//        ELog.i(TAG, "test");
//        List<File> list = ELog.queryLogFile(System.currentTimeMillis());
//        for (File file : list) {
//            try {
//                InputStream inputStream = new FileInputStream(file);
//                String result = ELog.getString(inputStream);
//                System.out.println(result);
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            }
//        }
        initData();
    }

    protected void initData() {
        listData = new ArrayList<String>();
        for (int i = 0; i < 50; i++) {
            listData.add("" +  i);
        }
        mAdapter.notifyDataSetChanged();
    }

    public class MAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new MViewHolder(inflater.inflate(R.layout.item_rv_divider_item, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            MViewHolder viewHolder = (MViewHolder) holder;
            viewHolder.txt.setText(listData.get(position));
        }

        @Override
        public int getItemCount() {
            return listData == null ? 0 : listData.size();
        }
    }

    public class MViewHolder extends RecyclerView.ViewHolder {
        public TextView txt;

        public MViewHolder(View itemView) {
            super(itemView);
            txt = (TextView) itemView.findViewById(R.id.txt);
        }
    }

}

