package guanaj.com.scrollerdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Scroller;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Scroller scroller;
    private LinearLayout llContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button startScrollby = (Button) findViewById(R.id.start_scrollby);
        Button startScrollto = (Button) findViewById(R.id.start_scrollto);

        llContent = (LinearLayout) findViewById(R.id.ll_content);

        TextView txt = (TextView) findViewById(R.id.txt);
        //初始化Scroller
        scroller = new Scroller(this);
        startScrollby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startScrollby();
            }
        });
        startScrollto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startScrollto();
            }
        });

    }

    private void startScrollby() {
        llContent.scrollBy(-100,0);

    }
    private void startScrollto() {
        llContent.scrollTo(0,0);

    }
}
