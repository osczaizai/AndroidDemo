package guanaj.com.library.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class ELog {

    private ELog() {
        throw new UnsupportedOperationException("you can't instantiate me...");
    }

    private static boolean logSwitch = true;
    private static boolean log2FileSwitch = false;
    private static char logFilter = 'v';
    private static String tag = "TAG";
    private static String dir = null;
    private static String dirName = "veni";
    private static Context mContext = null;


    public static Builder getBuilder(Context context) {
        mContext = context;
        System.out.println("getCacheDir");
        dir = mContext.getCacheDir().getPath() + File.separator + dirName + File.separator;
        return new Builder();
    }

    public static class Builder {

        private boolean logSwitch = true;
        private boolean log2FileSwitch = false;
        private char logFilter = 'v';
        private String tag = "TAG";

        public Builder setLogSwitch(boolean logSwitch) {
            this.logSwitch = logSwitch;
            return this;
        }

        public Builder setLog2FileSwitch(boolean log2FileSwitch) {
            this.log2FileSwitch = log2FileSwitch;
            return this;
        }

        public Builder setLogFilter(char logFilter) {
            this.logFilter = logFilter;
            return this;
        }

        public Builder setTag(String tag) {
            this.tag = tag;
            return this;
        }

        public void create() {
            ELog.logSwitch = logSwitch;
            ELog.log2FileSwitch = log2FileSwitch;
            ELog.logFilter = logFilter;
            ELog.tag = tag;
        }
    }

    /**
     * Verbose日志
     *
     * @param msg 消息
     */
    public static void v(String msg) {
        log(tag, msg, null, 'i');
    }


    /**
     * Verbose日志
     *
     * @param tag 标签
     * @param msg 消息
     */
    public static void v(String tag, String msg) {
        log(tag, msg, null, 'i');
    }

    /**
     * Verbose日志
     *
     * @param tag 标签
     * @param msg 消息
     * @param tr  异常
     */
    public static void v(String tag, String msg, Throwable tr) {
        log(tag, msg, tr, 'v');
    }

    /**
     * Debug日志
     *
     * @param msg 消息
     */
    public static void d(String msg) {
        log(tag, msg, null, 'd');
    }

    /**
     * Debug日志
     *
     * @param tag 标签
     * @param msg 消息
     */
    public static void d(String tag, String msg) {
        log(tag, msg, null, 'd');
    }

    /**
     * Debug日志
     *
     * @param tag 标签
     * @param msg 消息
     * @param tr  异常
     */
    public static void d(String tag, String msg, Throwable tr) {
        log(tag, msg, tr, 'd');
    }

    /**
     * Info日志
     *
     * @param msg 消息
     */
    public static void i(String msg) {
        log(tag, msg, null, 'i');
    }

    /**
     * Info日志
     *
     * @param tag 标签
     * @param msg 消息
     */
    public static void i(String tag, String msg) {
        log(tag, msg, null, 'i');
    }

    /**
     * Info日志
     *
     * @param tag 标签
     * @param msg 消息
     * @param tr  异常
     */
    public static void i(String tag, String msg, Throwable tr) {
        log(tag, msg, tr, 'i');
    }

    /**
     * Warn日志
     *
     * @param msg 消息
     */
    public static void w(String msg) {
        log(tag, msg, null, 'w');
    }

    /**
     * Warn日志
     *
     * @param tag 标签
     * @param msg 消息
     */
    public static void w(String tag, String msg) {
        log(tag, msg, null, 'w');
    }

    /**
     * Warn日志
     *
     * @param tag 标签
     * @param msg 消息
     * @param tr  异常
     */
    public static void w(String tag, String msg, Throwable tr) {
        log(tag, msg, tr, 'w');
    }

    /**
     * Error日志
     *
     * @param msg 消息
     */
    public static void e(String msg) {
        log(tag, msg, null, 'e');
    }

    /**
     * Error日志
     *
     * @param tag 标签
     * @param msg 消息
     */
    public static void e(String tag, String msg) {
        log(tag, msg, null, 'e');
    }

    /**
     * Error日志
     *
     * @param tag 标签
     * @param msg 消息
     * @param tr  异常
     */
    public static void e(String tag, String msg, Throwable tr) {
        log(tag, msg, tr, 'e');
    }

    /**
     * 根据tag, msg和等级，输出日志
     *
     * @param tag  标签
     * @param msg  消息
     * @param tr   异常
     * @param type 日志类型
     */
    private static void log(String tag, String msg, Throwable tr, char type) {
        if (msg == null || msg.isEmpty()) return;
        if (logSwitch) {
            if ('e' == type && ('e' == logFilter || 'v' == logFilter)) {
                printLog(tag, msg, tr, 'e');
            } else if ('w' == type && ('w' == logFilter || 'v' == logFilter)) {
                printLog(tag, msg, tr, 'w');
            } else if ('d' == type && ('d' == logFilter || 'v' == logFilter)) {
                printLog(tag, msg, tr, 'd');
            } else if ('i' == type && ('d' == logFilter || 'v' == logFilter)) {
                printLog(tag, msg, tr, 'i');
            }
            if (log2FileSwitch) {
                log2File(type, tag, msg + '\n' + Log.getStackTraceString(tr));
            }
        }
    }

    /**
     * 根据tag, msg和等级，输出日志
     *
     * @param tag  标签
     * @param msg  消息
     * @param tr   异常
     * @param type 日志类型
     */
    private static void printLog(final String tag, final String msg, Throwable tr, char type) {
        final int maxLen = 4000;
        for (int i = 0, len = msg.length(); i * maxLen < len; ++i) {
            String subMsg = msg.substring(i * maxLen, (i + 1) * maxLen < len ? (i + 1) * maxLen : len);
            switch (type) {
                case 'e':
                    Log.e(tag, subMsg, tr);
                    break;
                case 'w':
                    Log.w(tag, subMsg, tr);
                    break;
                case 'd':
                    Log.d(tag, subMsg, tr);
                    break;
                case 'i':
                    Log.i(tag, subMsg, tr);
                    break;
            }
        }
    }

    /**
     * 打开日志文件并写入日志
     *
     * @param type 日志类型
     * @param tag  标签
     * @param msg  信息
     **/
    private synchronized static void log2File(final char type, final String tag, final String msg) {
        Date now = new Date();

        String date = new SimpleDateFormat("MM-dd", Locale.getDefault()).format(now);
        final String fullPath = dir + date + ".txt";

        if (!FileUtils.createOrExistsFile(fullPath)) return;
        String time = new SimpleDateFormat("HH:mm:ss.SSS", Locale.getDefault()).format(now);
        final String dateLogContent = "["+time + "]-[" + type + "]-" + tag + ":" + msg + '\n';
        new Thread(new Runnable() {
            @Override
            public void run() {
                BufferedWriter bw = null;
                try {
                    bw = new BufferedWriter(new FileWriter(fullPath, true));
                    bw.write(dateLogContent);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    CloseUtils.closeIO(bw);
                }
            }
        }).start();
    }


    public synchronized static void clearFileBeforeOfDay(int beforeOfDay) {
        Date date = new Date();
        //减去需要删除的天数
        List<String> notDeleteFiles = new ArrayList<>();
        long temp;
        long time;
        String dateStr;
        SimpleDateFormat format = new SimpleDateFormat("MM-dd", Locale.getDefault());
        String fileName;
        for (int i = 0; i < beforeOfDay; i++) {
            temp = i * 24 * 60 * 60 * 1000;
            time = date.getTime() - temp;
            date = new Date(time);
            dateStr = format.format(date);
            fileName = dateStr + ".txt";
            notDeleteFiles.add(fileName);
        }

        List<File> deleteFiles = new ArrayList<>();
        List<File> files = FileUtils.listFilesInDir(dir);
        boolean isDelete = true;
        for (File file : files) {
            for (String notDeleteFile : notDeleteFiles) {
                if (file.getName().equals(notDeleteFile)) {
                    isDelete = false;
                    break;
                }
            }
            if (isDelete) {
                deleteFiles.add(file);
            }

        }
        if (deleteFiles.size() > 0) {
            for (File file : deleteFiles) {
                file.delete();
            }
        }


    }


    public synchronized static List<File> queryLogFile(long time) {

        File dirFile = new File(dir);
        File[] files = dirFile.listFiles();
        List<File> resultFiles = new ArrayList<>();

        if (time == 0) {
            for (File file : files) {
                resultFiles.add(file);
            }
        } else {

            SimpleDateFormat format = new SimpleDateFormat("MM-dd", Locale.getDefault());


            Date date = new Date(time);
            String dateStr = format.format(date);
            String fileName = dateStr + ".txt";
            //获得dir目录下的文件


            if (files != null && files.length > 0) {
                //遍历下面的所有文件
                for (File file : files) {

                    if (file.getName().equals(fileName)) {
                        resultFiles.add(file);
                    }

                }
            }
        }

        return resultFiles;

    }

    public static String getString(InputStream inputStream) {

        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(inputStream, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuilder stringBuilder = new StringBuilder("");
        String line = "";
        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return stringBuilder.toString();

    }


}
