package guanaj.com.library;


import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by guanaj on 2017/3/29.
 */

public class DividerItemDecoration extends RecyclerView.ItemDecoration {
    private double leftAndRight;
    private double topAndButtom;

    public DividerItemDecoration(double leftAndRight, double topAndButtom) {
        this.leftAndRight = leftAndRight;
        this.topAndButtom = topAndButtom;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDraw(c, parent, state);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDrawOver(c, parent, state);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
//        super.getItemOffsets(outRect, view, parent, state);
    }


}
