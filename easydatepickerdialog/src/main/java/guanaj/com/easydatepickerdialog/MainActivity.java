package guanaj.com.easydatepickerdialog;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button showButton = (Button) findViewById(R.id.show_button);
        Button hidButton = (Button) findViewById(R.id.hide_button);
        final TextView showDate = (TextView) findViewById(R.id.show_date);



        showButton.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                EasyDatePickerDialog easyDatePickerDialog =    new EasyDatePickerDialog(MainActivity.this, 0, new EasyDatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker DatePicker, int year, int monthOfYear,
                                          int dayOfMonth) {
                        String textString = String.format("选择年月：%d-%d\n", year,
                                monthOfYear + 1);
                        showDate.setText(textString);
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE),false);

                easyDatePickerDialog.setMaxDate(2018,1,1);
                easyDatePickerDialog.setMinDate(2000,1,1);
                easyDatePickerDialog.show();
            }
        });
        hidButton.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                EasyDatePickerDialog easyDatePickerDialog =    new EasyDatePickerDialog(MainActivity.this, 0, new EasyDatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker DatePicker, int year, int monthOfYear,
                                          int dayOfMonth) {
                        String textString = String.format("选择年月：%d-%d\n", year,
                                monthOfYear + 1);
                        showDate.setText(textString);
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE),true);

                easyDatePickerDialog.setMaxDate(2018,1,1);
                easyDatePickerDialog.setMinDate(2000,1,1);
                easyDatePickerDialog.show();
            }
        });


    }

}
