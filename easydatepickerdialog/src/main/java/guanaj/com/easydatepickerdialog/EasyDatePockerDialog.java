package guanaj.com.easydatepickerdialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import java.lang.reflect.Field;
import java.util.Calendar;

/**
 * Created by guanaj on 2017/3/7.
 */

class EasyDatePickerDialog extends AlertDialog implements DialogInterface.OnClickListener,
        DatePicker.OnDateChangedListener {

    private static final String YEAR = "year";
    private static final String MONTH = "month";
    private static final String DAY = "day";


    private final DatePicker mDatePicker;
    private final OnDateSetListener mDateSetListener;
    private final Calendar mCalendar;
    private boolean mHidDay =false;
    private Context mContext;


    public interface OnDateSetListener {
        void onDateSet(DatePicker startDatePicker, int startYear, int startMonthOfYear, int startDayOfMonth);
    }


    private boolean mTitleNeedsUpdate = true;



    public EasyDatePickerDialog(Context context,
                                OnDateSetListener callBack,
                                int year,
                                int monthOfYear,
                                int dayOfMonth,boolean hidDay) {
        this(context, 0, callBack, year, monthOfYear, dayOfMonth,hidDay);
    }


    public EasyDatePickerDialog(Context context, int theme, OnDateSetListener listener, int year,
                                int monthOfYear, int dayOfMonth,boolean hidDay) {
        super(context,  theme);

        mDateSetListener = listener;
        mCalendar = Calendar.getInstance();
        mHidDay = hidDay;
        mContext = getContext();
        final LayoutInflater inflater = LayoutInflater.from(mContext);
        final View view = inflater.inflate(R.layout.easydatepickterview, null);
        setButton(BUTTON_POSITIVE, "确定", this);
        setButton(BUTTON_NEGATIVE, "取消", this);
        //setIcon(0); //移除按钮效果
        setView(view);


        mDatePicker = (DatePicker) view.findViewById(R.id.easydatePicker);
        mDatePicker.init(year, monthOfYear, dayOfMonth, this);
        if (hidDay) {
            hideDayView(mDatePicker);
        }

    }

    private void hideDayView(DatePicker mDatePicker) {
        try {
            /* android5.0以上 */
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int daySpinnerId = Resources.getSystem().getIdentifier("day", "id", "android");
                if (daySpinnerId != 0) {
                    View daySpinner = mDatePicker.findViewById(daySpinnerId);
                    if (daySpinner != null) {
                        daySpinner.setVisibility(View.GONE);
                    }
                }
            } else {
                Field[] datePickerfFields = mDatePicker.getClass().getDeclaredFields();
                for (Field datePickerField : datePickerfFields) {
                    if ("mDaySpinner".equals(datePickerField.getName()) || ("mDayPicker").equals(datePickerField.getName())) {
                        datePickerField.setAccessible(true);
                        Object dayPicker = new Object();
                        try {
                            dayPicker = datePickerField.get(mDatePicker);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                        ((View) dayPicker).setVisibility(View.GONE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    @Override
    public void onDateChanged(DatePicker view, int year, int month, int day) {
        if (view.getId()==R.id.easydatePicker) {
            mDatePicker.init(year, month, day, this);
            //可根据需要在此处更新title
        }

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case BUTTON_POSITIVE:
                if (mDateSetListener != null) {
                    // Clearing focus forces the dialog to commit any pending
                    // changes, e.g. typed text in a NumberPicker.
                    mDatePicker.clearFocus();
                    mDateSetListener.onDateSet(mDatePicker, mDatePicker.getYear(),
                            mDatePicker.getMonth(), mDatePicker.getDayOfMonth());
                }
                break;
            case BUTTON_NEGATIVE:
                cancel();
                break;
        }
    }


    public DatePicker getDatePicker() {
        return mDatePicker;
    }


    public void updateDate(int year, int monthOfYear, int dayOfMonth) {
        mDatePicker.updateDate(year, monthOfYear, dayOfMonth);
    }

    @Override
    public Bundle onSaveInstanceState() {
        final Bundle state = super.onSaveInstanceState();
        state.putInt(YEAR, mDatePicker.getYear());
        state.putInt(MONTH, mDatePicker.getMonth());
        state.putInt(DAY, mDatePicker.getDayOfMonth());
        return state;
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final int year = savedInstanceState.getInt(YEAR);
        final int month = savedInstanceState.getInt(MONTH);
        final int day = savedInstanceState.getInt(DAY);
        mDatePicker.init(year, month, day, this);
    }

    public void setMaxDate(int year,int month,int day){
        mCalendar.set(year,month-1,day);
        mDatePicker.setMaxDate(mCalendar.getTimeInMillis());
    }
    public void setMinDate(int year,int month,int day){
        mCalendar.set(year,month-1,day);
        mDatePicker.setMinDate(mCalendar.getTimeInMillis());
    }


}
